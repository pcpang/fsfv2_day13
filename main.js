/**
 * Created by pohchye on 15/7/2016.
 */
// Create the Express app
var express=require("express");
var app=express();

// Path module
var path=require("path");

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': false}));
app.use(bodyParser.json());

// Load mysql module
var mysql=require("mysql");
// Create mysql connection pool
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "pohchye",
    password: "pohchye",
    database: "employees",
    connectionLimit: 4
});



// Middleware
// Day 14
var Employee = function (first_name, last_name, gender, birth_date, hire_date) {
    this.firstName = first_name;
    this.lastName = last_name;
    this.gender = gender;
    this.birthDate = birth_date;
    this.hireDate = hire_date;
    // this.save = function () {
    //     console.info("Save Employee");
    // }
};
// Prototype function is shared by the Employees objects, saving memory space
Employee.prototype.save = function () {
    console.info("Saving Employee");
};
// Can use to set the default value for properties too
// End of Day 14

// Filter using 2 tables
const INSERTSQL = "insert into employees (first_name, last_name, gender, birth_date, " +
    "hire_date) values (?,?,?,?,?)";
app.post("/api/employee/save", function(req, res) {
    console.info("SQL: " + INSERTSQL);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("Error connection");
            res.status(500).send(JSON.stringify(err));
            return;
        }
        console.info("Good connection");
        var gender = req.body.gender == 'male' ? 'M' : 'F'; //enum fields
        var birthDate = req.body.birthday.substring(0, req.body.birthday.indexOf('T'));
        var hireDate = req.body.hiredate.substring(0, req.body.hiredate.indexOf('T'));
        // Day 14
        var employee = new Employee(req.body.firstname, req.body.lastname, gender, birthDate, hireDate);
        console.info("New Employee: " + employee.firstName);
        // End of Day 14
        connection.query(INSERTSQL,[req.body.firstname, req.body.lastname, gender, birthDate, hireDate],
            function (err,result) {
                connection.release();
                if (err) {
                    console.info("Query Error");
                    res.status(500).send("Query Error: " + JSON.stringify(err));
                    return;
                }
                // console.info(result);
                console.info("Result Insert ID: " + result.insertId);
                res.status(202).json({abc: "/api/employee/" + result.insertId});
            }
        );
    });
});

// Serve public files
app.use(express.static(__dirname + "/public"));
// - For bower_components that is on the root directory instead of public
// app.use("/bower_components",express.static(path.join(__dirname, "bower_components")));
// app.use(express.static(path.join(__dirname,"public")));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3013
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});