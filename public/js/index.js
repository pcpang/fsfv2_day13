/**
 * Created by pohchye on 15/7/2016.
 */

(function () {
    angular
        .module("RegApp", [])
        .controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"]; // to remove the white spaces, does not affect the logic
    function RegCtrl($http) {
        var vm = this;

        vm.employee = {};
        vm.employee.firstname = "";
        vm.employee.lastname = "";
        vm.employee.gender  = "";
        vm.employee.birthday = "";
        vm.employee.hiredate = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.register = function () {
            $http.post("/api/employee/save", vm.employee)
            .then(function (result) {
                console.info("Result: " + result);
                vm.status.message = "Inserted Successfully";
                vm.status.code = 202;
                vm.newEmployeeURL = result.data.abc;
                console.info("URL: " + vm.newEmployeeURL);
                // vm.result = result.data.url;
            })
            .catch(function () {
                // console.info(error);
                vm.status.message = "Error occurred";
                vm.status.code = 400;
                // vm.error = error.data;
            })
        };
    }
})();